package com.dtrung98.navigation.uicontainer;

import com.ldt.navigation.PresentStyle;

public class ExpandStaticContainer extends ExpandContainer {
  @Override
  public int defaultTransition() {
    return PresentStyle.NONE;
  }
}