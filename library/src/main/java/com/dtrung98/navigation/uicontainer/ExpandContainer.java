package com.dtrung98.navigation.uicontainer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ldt.navigation.NavigationFragment;
import com.ldt.navigation.R;

public class ExpandContainer extends AnimatorUIContainer {
  public View provideLayout(Context context, LayoutInflater inflater, ViewGroup viewGroup, int providedSubContainerId) {
    // provide container layout
    // provide sub container layout
    View v = inflater.inflate(R.layout.aninator_expand_container, viewGroup, false);
    v.findViewById(R.id.sub_container).setId(providedSubContainerId);
    return v;
  }

  @Override
  public int defaultDuration() {
    return 325;
  }

  @Override
  public int defaultTransition() {
    return NavigationFragment.PRESENT_STYLE_DEFAULT;
  }
}